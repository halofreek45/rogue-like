﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {

    public static SoundController Instance;
    public AudioSource soundEffect;
    public AudioSource music;

    private float lowPitchRange = 0.05f;
    private float highPitchRange = 1.05f;

	void Awake () {
	if(Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
	}

    public void PlaySingle(params AudioClip[] clips)
    {
        RandomizeSoundEffects(clips);
        soundEffect.Play();
    }
	
    private void RandomizeSoundEffects(AudioClip[] clips)
    {
        int randomSoundIndex = Random.Range(0, clips.Length);
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);


        soundEffect.clip = clips[randomSoundIndex];
    }
}
