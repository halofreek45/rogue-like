using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public int PlayerCurrentHealth = 50;
    public AudioClip gameOverSound;
    public GameObject MainImage;

    private BoardController boardController;
    private List<Enemy> enemies;
    private GameObject levelImage;
    private Text levelText;
    private bool settingUpGame;
    private int secondsUntilLevelStart = 2;
    private int currentLevel = 1;
    private int playerUp = 5000000;
    public int selectedCharacter;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
    }

    void Start()
    {
        InitializeGame();
    }

    private void InitializeGame()
    { 
        settingUpGame = true;
        MainImage = GameObject.Find("Main Menu");
        MainImage.SetActive(true);
        levelImage = GameObject.Find("Level Image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        levelText.text = "Day " + currentLevel;
        levelImage.SetActive(true);
        enemies.Clear();
        boardController.SetupLevel(currentLevel);
        Invoke("DisableLevelImage", secondsUntilLevelStart);
        if(currentLevel > 1)
        {
            MainImage = GameObject.Find("Main Menu");
            MainImage.SetActive(false);
        }
    }
    public void SelectPlayer1()
    {
        boardController.selectedCharacter();
        MainImage.SetActive(false);
    }
    public void SelectPlayer2()
    {
        boardController.selectedCharacter2();
        selectedCharacter = 1;
        MainImage.SetActive(false);
    }

    public void DisableLevelImage()
    {

        levelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;
    }

    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        InitializeGame();
    }

    void Update()
    {
        if (isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }
        StartCoroutine(MoveEnemies());
    }

    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach (Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }

        areEnemiesMoving = false;
        isPlayerTurn = true;
    }

    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }
    public int GetHighscore()
    {
        int BestScore = PlayerPrefs.GetInt("highscore", 0);
        if (currentLevel > BestScore)
        {
            PlayerPrefs.SetInt("highscore", currentLevel);
        }
        return PlayerPrefs.GetInt("highscore", 0);
    }

    public void GameOver()
    {
        int highScore = GetHighscore();
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);
        levelText.text = "You starved after " + currentLevel + " days..."+ "HighScore: " + highScore;
        levelImage.SetActive(true);
        enabled = false;
    }
 
}